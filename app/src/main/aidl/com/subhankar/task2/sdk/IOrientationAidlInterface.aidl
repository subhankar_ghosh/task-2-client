// IOrientationAidlInterface.aidl
package com.subhankar.task2.sdk;
import  com.subhankar.task2.sdk.IMyAidlCbInterface;
// Declare any non-default types here with import statements

interface IOrientationAidlInterface {
    /**
     * Demonstrates some basic types that you can use as parameters
     * and return values in AIDL.
     */
        String orientation();
        void registerCb(IMyAidlCbInterface cb);
        void unRegisterCb(IMyAidlCbInterface cb);
}