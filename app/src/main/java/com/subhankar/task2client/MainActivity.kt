package com.subhankar.task2client

import android.app.Service
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import androidx.appcompat.app.AppCompatActivity
import com.subhankar.task2.sdk.IMyAidlCbInterface
import com.subhankar.task2.sdk.IOrientationAidlInterface
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private var iOrientationAidlInterface: IOrientationAidlInterface? = null
    internal var iMyAidlCbInterface : IMyAidlCbInterface? = null
    private val serverAppPackage = "com.subhankar.task2"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()
    }

    private fun init() {
        if (iOrientationAidlInterface == null) {
            val intent = Intent(IOrientationAidlInterface::class.java.name)

            /*this is service name which has been declared in the server's manifest file in service's intent-filter*/
            intent.action = "com.subhankar.task2.MyOrientationService"
            intent.setPackage(serverAppPackage)
            // binding to remote service
            bindService(intent, serviceConnection, Service.BIND_AUTO_CREATE)


            intent.action = "com.subhankar.task2.MyOrientationService.cb"
            intent.setPackage(serverAppPackage)
            bindService(intent, secondaryConnection, Service.BIND_AUTO_CREATE)
        }
    }

    private val serviceConnection = object : ServiceConnection {
        override fun onServiceConnected(componentName: ComponentName, iBinder: IBinder) {
            println("@@@ Attached")
            iOrientationAidlInterface = IOrientationAidlInterface.Stub.asInterface(iBinder)
            tv_sensor_data.text = "Attached."
            iOrientationAidlInterface?.registerCb(mCallback)
        }

        override fun onServiceDisconnected(componentName: ComponentName) {
            println("@@@ Attached")
            iOrientationAidlInterface = null
            tv_sensor_data.text = "Disconnected."
        }
    }

    private val secondaryConnection = object : ServiceConnection {

        override fun onServiceConnected(className: ComponentName, service: IBinder) {
            // Connecting to a secondary interface is the same as any
            // other interface.
            iMyAidlCbInterface = IMyAidlCbInterface.Stub.asInterface(service)
        }

        override fun onServiceDisconnected(className: ComponentName) {
            iMyAidlCbInterface = null
        }
    }


    private val mCallback = object : IMyAidlCbInterface.Stub() {
        /**
         * This is called by the remote service regularly to tell us about
         * new values.  Note that IPC calls are dispatched through a thread
         * pool running in each process, so the code executing here will
         * NOT be running in our main thread like most other things -- so,
         * to update the UI, we need to use a Handler to hop over there.
         */
        override fun showData(data: String?) {
            println("@@@ " + data)
            tv_sensor_data.text = data
        }
    }

    override fun onResume() {
        super.onResume()
        if (iOrientationAidlInterface == null) {
            init()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        unbindService(serviceConnection)
    }
}